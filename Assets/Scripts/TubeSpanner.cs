﻿using UnityEngine;
using System.Collections;

public class TubeSpanner : MonoBehaviour {

	public GameObject tube;
	float maxPos = 3f;
	float minPos = -3f;
	public float delayTimer = 2f;
	float timer;

	// Use this for initialization
	void Start () {
		timer = delayTimer;
	}
	
	// Update is called once per frame
	void Update () {
		timer -= Time.deltaTime;
		if(timer <= 0){
			Vector3 tubePos = new Vector3 ( transform.position.x, Random.Range (maxPos, minPos), transform.position.z);
			Instantiate (tube, tubePos, transform.rotation);
			timer = delayTimer;
		}
	
	}
}
