﻿using UnityEngine;
using System.Collections;

public class BallonMovement : MonoBehaviour {

	Vector3 velocity = Vector3.zero;
	public Vector3 gravity;
	public Vector3 flapVelocity;
	public float maxSpeed = 5f;

	public Transform balloonChecker;
	public Transform balloonChecker1; 

	bool didFlap = false;

	public UIManager ui;

	// Use this for initialization
	void Start () {		
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space) || Input.GetMouseButtonDown (0)) {
			didFlap = true;
		}	
		if(transform.position.y < balloonChecker.transform.position.y || transform.position.y > balloonChecker1.transform.position.y){
			Application.LoadLevel(Application.loadedLevel);
		}
	}

	void FixedUpdate(){
		velocity += gravity * Time.deltaTime;
		if (didFlap == true) {
			didFlap = false;
			velocity.y += flapVelocity.y;
		}
		velocity = Vector3.ClampMagnitude (velocity, maxSpeed);

		transform.position += velocity * Time.deltaTime;
	}

	void OnCollisionEnter2D(Collision2D col){
		if (col.gameObject.tag == "Tubes") {
			Destroy (gameObject);
			ui.GameOverActivated ();
		}
	}

}
